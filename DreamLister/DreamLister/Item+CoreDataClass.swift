//
//  Item+CoreDataClass.swift
//  DreamLister
//
//  Created by Dustin Hullett on 10/7/16.
//  Copyright © 2016 Dustin Hullett. All rights reserved.
//

import Foundation
import CoreData


public class Item: NSManagedObject {
    
    public override func awakeFromInsert() {
        super.awakeFromInsert()
        
        self.created = NSDate()
    }
}
