//
//  ItemCell.swift
//  DreamLister
//
//  Created by Dustin Hullett on 10/7/16.
//  Copyright © 2016 Dustin Hullett. All rights reserved.
//

import UIKit

class ItemCell: UITableViewCell {

    @IBOutlet weak var thumb: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var details: UILabel!
    @IBOutlet weak var type: UILabel!
    
    func configureCell(item: Item) {
        
        title.text = item.title
        price.text = "$\(item.price)"
        details.text = item.details
        thumb.image = item.toimage?.image as? UIImage
        type.text = item.toItemType?.type
    }
    
}
